package com.loginprac.loginprac.controller;

import com.loginprac.loginprac.entity.Client;
import com.loginprac.loginprac.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/register")
    public Client registerClient(@RequestBody Client client){

        return clientService.registerClient(client);
    }

    @GetMapping("/login")
    public Client loginClient(@RequestBody Client client){
        return clientService.loginClient(client.getEmail(), client.getPassword());
    }

}
