package com.loginprac.loginprac.repository;

import com.loginprac.loginprac.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepo extends JpaRepository<Client, Long> {
    @Query(value = "SELECT * FROM CLIENT where EMAIL = ?1", nativeQuery = true)
    Optional<Client> findClientByEmail(String email);

    @Query(value = "SELECT password FROM CLIENT where EMAIL = ?1", nativeQuery = true)
    Optional<String> findClientPasswordbyEmail(String password);

    Client findByEmailAndPassword(String email, String password);

}
