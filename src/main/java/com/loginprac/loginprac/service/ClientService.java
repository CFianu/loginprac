package com.loginprac.loginprac.service;

import com.loginprac.loginprac.entity.Client;



public interface ClientService {
    public Client registerClient(Client client);

//    Client loginClient(Client client);

    public Client loginClient(String email, String password);
}
