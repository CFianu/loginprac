package com.loginprac.loginprac.service;

import com.loginprac.loginprac.entity.Client;
import com.loginprac.loginprac.repository.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientRepo clientRepo;


    @Override
    public Client registerClient(Client client) {
        Optional<Client> email = clientRepo.findClientByEmail(client.getEmail());
        if(email.isPresent()){
            throw new IllegalStateException("This user already exists");
        }
        return clientRepo.save(client);
    }





    public Client loginClient(String email, String password){
        Client client = clientRepo.findByEmailAndPassword(email,password);

        //System.out.println(client.getPassword());
        return client;
    }
}
